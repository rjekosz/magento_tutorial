document.observe('dom:loaded', function () {

    /* HEADER STORE LOCATOR/CHANGER */

    let main_handler = $$('div.top-locations__handler')[0],
        change_handler = $$(".top-locations-btn")[0],
        map_wrapper = $$('div.top-locations-left')[0],
        list_wrapper = $$('div.top-locations-list')[0],
        body = $$('body')[0],
        page_overlay = $("overlay");

    main_handler.on('click', function (event, el) {

        if (el.hasClassName('locations-popup-opened') === false) {
            el.next().appear({
                duration: 0.3,
                from: 0,
                to: 1
            });

            page_overlay.appear({
                duration: 0.2,
                from: 0,
                to: 1
            });

            el.addClassName('locations-popup-opened');
            body.addClassName('scroll-block');
        } else {
            el.next().fade({
                duration: 0.3,
                from: 1,
                to: 0
            });
            page_overlay.fade({
                duration: 0.2,
                from: 1,
                to: 0
            });

            el.removeClassName('locations-popup-opened');
            body.removeClassName('scroll-block');
        }
    });

    change_handler.on('click', function (event, el) {

        el.fade({
            duration: 0.5,
            from: 1,
            to: 0
        });

        function show_input() {
            el.next().appear({
                duration: 0.2,
                from: 0,
                to: 1
            })
        }

        show_input.delay(0.5);


        if (el.hasClassName('locations-change-opened') === false) {

            [map_wrapper, list_wrapper].each(function (el) {
                el.appear({
                    duration: 0.3,
                    from: 0,
                    to: 1
                });
            });
            el.addClassName('locations-change-opened');
        } else {
            [map_wrapper, list_wrapper].each(function (el) {
                el.fade({
                    duration: 0.3,
                    from: 1,
                    to: 0
                });
            });

            el.removeClassName('locations-change-opened');
        }
    });

});

jQuery(document).ready(function() {
    /* SELECTS */
    if (jQuery("select").length !== 0) {
        jQuery("select").selectric();
    }
});
